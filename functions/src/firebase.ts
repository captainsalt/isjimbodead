import * as admin from "firebase-admin";
import * as functions from "firebase-functions";

admin.initializeApp(functions.config().firebase);

const db = admin.firestore();
const tokenCollection = db.collection("tokens")

export const addTokens = async (twitchId: string, data: any) => {
    try {
        await tokenCollection.doc(twitchId).set(data);
    } catch (error) {
        throw error;
    }
};

export const updateTokens = async (twitchId: string, refreshToken: string, accessToken: string) => {
    try {
        await tokenCollection.doc(twitchId).update({
            refresh_token: refreshToken,
            access_token: accessToken
        });
    } catch (error) {
        throw error;
    }
};

export const getTokens = async (channelId: string) => {
    try {
        const result = (await tokenCollection.doc(channelId).get()).data();

        if (!result) throw Error("No oauth token found in database");

        return result;
    } catch (error) {
        throw error;
    }
}

