import * as env from "dotenv";
env.config();

import * as functions from "firebase-functions";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as db from "./firebase"
import { isAceBanned, generateTokens, getTwitchId } from "./twitch-service";
import dayjs from "./dayjs";

const app = express();
const main = express();

main.use("/", app);
main.use(bodyParser.json());

app.get("/auth", async (req, res) => {
    try {
        const queries = req.originalUrl.split("?")[1].split("&");
        let code = "";

        for (const query of queries) {
            const [name, value] = query.split("=");
            if (name === "code") code = value;
        }

        const tokens = await generateTokens(code);
        const userId = await getTwitchId(tokens.access_token);

        await db.addTokens(userId, tokens);

        res.end("You're good to go");
    } catch (error) {
        res.status(500).send("Authentication error");
        console.error(error);
    }
});

app.get("/", async (req, res) => {
    try {
        const nightbotHeaders = req.header("Nightbot-Channel")?.split("&");
        let channelId = "";

        if (!nightbotHeaders) {
            throw Error("No nightbot headers in request");
        }

        for (const val of nightbotHeaders) {
            const [key, value] = val.split("=");
            if (key === "providerId") channelId = value;
        }

        if (!channelId) {
            throw Error("Channel id not found in headers");
        }

        const response = await isAceBanned(channelId);

        if (response.data.data?.length) {
            let message = "";
            const data = response.data.data[0];

            if (data.expires_at) {
                const fromNow = dayjs(data.expires_at).fromNow();
                message = `Ace is in jail. His release date is ${fromNow}`;
            }
            else {
                message = "Ace is banned Hotdogjoubu";
            }

            res.status(200).send(message);
        }
        else {
            res.status(200).send("Ace is alive...for now PokeSlow");
        }
    }
    catch (error) {
        res.status(500).send("Internal Error");
        console.error(error);
    }
});

export const jimboStatus = functions.https.onRequest(main);
