import axios from "axios";
import * as db from "./firebase";

export const twitchClient = axios.create({
    baseURL: "https://api.twitch.tv/helix/",
    headers: { "Client-ID": process.env.client_id }
});

export const isAceBanned = async (channelId: string) => {
    const aceId = "89104277";
    const requestUrl = `moderation/banned?broadcaster_id=${channelId}&user_id=${aceId}`;
    const { access_token } = await db.getTokens(channelId);

    const clientOptions = {
        headers: {
            "Authorization": `Bearer ${access_token}`
        }
    };

    try {
        const response = await twitchClient.get(requestUrl, clientOptions).catch(err => err);

        if (response.response?.status === 401) {
            const newToken = await refreshTokens(channelId);
            clientOptions.headers.Authorization = `Bearer ${newToken}`;
            return await twitchClient.get(requestUrl, clientOptions);
        }

        return response;
    } catch (error) {
        throw error;
    }
};

const refreshTokens = async (channelId: string) => {
    try {
        const { refresh_token } = await db.getTokens(channelId);

        const response = await twitchClient.post(`https://id.twitch.tv/oauth2/token\
?grant_type=refresh_token\
&refresh_token=${encodeURI(refresh_token)}\
&client_id=${process.env.client_id}\
&client_secret=${process.env.client_secret}`);

        const { access_token: newAccessToken, refresh_token: newRefreshToken } = response.data;

        await db.updateTokens(channelId, newRefreshToken, newAccessToken);
        return newAccessToken
    } catch (error) {
        throw error;
    }
};

export const generateTokens = async (code: string) => {
    try {
        const twitchRes = await axios.post(`https://id.twitch.tv/oauth2/token\
?client_id=${process.env.client_id}\
&client_secret=${process.env.client_secret}\
&code=${code}\
&grant_type=authorization_code\
&redirect_uri=${process.env.redirect}`);

        const { access_token, id_token, refresh_token } = twitchRes.data;
        return { access_token, id_token, refresh_token };
    } catch (error) {
        throw error;
    }
}

export const getTwitchId = async (oauthToken: string) => {
    try {
        const response = await axios.get("https://id.twitch.tv/oauth2/userinfo", {
            headers: {
                "Authorization": `Bearer ${oauthToken}`
            }
        });

        return response.data.sub;
    } catch (error) {
        throw error;
    }
};
